import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainModule } from './main/main.module';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APIConfig } from './main/services/config';
import { CarouselModule } from 'ngx-owl-carousel-o';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'content/list-of-gif',
    pathMatch: 'full'
  },
  {
    path: 'content',
    loadChildren: () =>
      import('./main/content/content.module').then(m => m.ContentModule)
  }
];
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MainModule,
    CarouselModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [APIConfig],
  bootstrap: [AppComponent]
})
export class AppModule {}
