import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MatDividerModule } from "@angular/material/divider";
import { MatListModule } from "@angular/material/list";
import { NavbarComponent } from "./navbar.component";

@NgModule({
  declarations: [NavbarComponent],
  imports: [RouterModule, MatListModule, MatDividerModule],
  exports: [NavbarComponent]
})
export class NavbarModule {}
