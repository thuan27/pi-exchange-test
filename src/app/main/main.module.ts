import { RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NavbarModule } from './navbar/navbar.module';
import { ContentModule } from './content/content.module';

@NgModule({
  declarations: [MainComponent],
  imports: [NavbarModule, ContentModule],
  exports: [MainComponent],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: []
})
export class MainModule {}
