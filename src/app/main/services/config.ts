import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class APIConfig {
  public API_BASE = environment.API.apiBase + '/v1/gifs';

  public LIST_OF_TRENDING = this.API_BASE + '/trending';
  public SEARCH_LIST = this.API_BASE + '/search';
}
