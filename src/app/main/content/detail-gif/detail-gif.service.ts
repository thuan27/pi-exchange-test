import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APIConfig } from '../../services/config';
import { ResDetailProps } from '../../../../types/trending.interface';
import { environment } from './../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class DetailGifService {
  constructor(private http: HttpClient, private apiConfig: APIConfig) {}

  getListSearch(id: string): Observable<ResDetailProps> {
    let paramsRequest = new HttpParams().set('api_key', environment.API.key);
    return this.http.get<ResDetailProps>(`${this.apiConfig.API_BASE}/${id}`, {
      params: paramsRequest
    });
  }
}
