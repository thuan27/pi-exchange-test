import { Component, OnInit } from '@angular/core';
import { DetailGifService } from './detail-gif.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ItemDetailProps, ResDetailProps } from 'src/types/trending.interface';

@Component({
  selector: 'app-detail-gif',
  templateUrl: './detail-gif.component.html',
  styleUrls: ['./detail-gif.component.scss'],
  providers: [DetailGifService]
})
export class DetailGifComponent implements OnInit {
  private routeSub: Subscription;
  private idDetail: string;
  itemDetail: ItemDetailProps;

  constructor(
    private _detailGifService: DetailGifService,
    private activeRoute: ActivatedRoute
  ) {
    this.routeSub = this.activeRoute.params.subscribe(params => {
      this.idDetail = params.id;
    });
  }

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    this._detailGifService.getListSearch(this.idDetail).subscribe(
      (item: ResDetailProps) => {
        this.itemDetail = { ...item.data };
      },
      err => {
        console.log(err);
      }
    );
  }
}
