import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { APIConfig } from '../../services/config';
import {
  ParamsListOfTrendingProps,
  DataTrendingProps,
  ParamsSearchListProps
} from '../../../../types/trending.interface';
import { Observable } from 'rxjs';

@Injectable()
export class ListOfGifService {
  constructor(private http: HttpClient, private apiConfig: APIConfig) {}

  getListOfTrending(
    params: ParamsListOfTrendingProps
  ): Observable<DataTrendingProps> {
    let paramsRequest = new HttpParams()
      .set('api_key', params.api_key)
      .set('limit', params.limit);

    return this.http.get<DataTrendingProps>(this.apiConfig.LIST_OF_TRENDING, {
      params: paramsRequest
    });
  }

  getListSearch(params: ParamsSearchListProps): Observable<DataTrendingProps> {
    let paramsRequest = new HttpParams()
      .set('api_key', params.api_key)
      .set('q', params.q)
      .set('limit', params.limit)
      .set('offset', params.offset);

    return this.http.get<DataTrendingProps>(this.apiConfig.SEARCH_LIST, {
      params: paramsRequest
    });
  }
}
