import { Component, OnInit } from "@angular/core";
import { ListOfGifService } from "./list-of-gif.service";
import { environment } from "./../../../../environments/environment";
import {
  ItemTrendingProps,
  DataTrendingProps
} from "src/types/trending.interface";
import { SlidesOutputData, OwlOptions } from "ngx-owl-carousel-o";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-list-of-gif",
  templateUrl: "./list-of-gif.component.html",
  styleUrls: ["./list-of-gif.component.scss"],
  providers: [ListOfGifService]
})
export class ListOfGifComponent implements OnInit {
  // store list of trending
  listOfTrending: DataTrendingProps;
  // store list search
  listSearch: DataTrendingProps;
  // default search
  searchForm: FormGroup;
  //default paginagtion
  pageSize: number = 10;
  pageIndex: number = 0;

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ["Prev", "Next"],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  };

  constructor(
    private _listOfGifService: ListOfGifService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.buildForm();
    this.getListOfTrending();
  }

  private buildForm() {
    this.searchForm = this.formBuilder.group({
      text: ""
    });
  }

  getListOfTrending() {
    const params = {
      api_key: environment.API.key,
      limit: this.pageSize
    };
    this._listOfGifService.getListOfTrending(params).subscribe(
      (data: DataTrendingProps) => {
        this.listOfTrending = { ...data };
      },
      err => {
        console.log(err);
      }
    );
  }

  onSearch() {
    const params = {
      api_key: environment.API.key,
      q: this.searchForm.controls.text.value,
      limit: this.pageSize,
      offset: this.pageIndex
    };
    this._listOfGifService.getListSearch(params).subscribe(
      (data: DataTrendingProps) => {
        this.listSearch = { ...data };
        console.log(this.listSearch);
      },
      err => {
        console.log(err);
      }
    );
  }

  pageEvent(event: any) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.onSearch();
  }

  goToDetail(id: string) {
    this.router.navigateByUrl(`content/list-of-trending/detail/${id}`);
  }

  showUp() {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }
}
