import { CardComponent } from "./../components/card/card.component";
import { ListOfGifComponent } from "./list-of-gif/list-of-gif.component";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";

import { ContentComponent } from "./content.component";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import {
  MatFormFieldModule,
  MAT_FORM_FIELD_DEFAULT_OPTIONS
} from "@angular/material/form-field";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CarouselModule } from "ngx-owl-carousel-o";
import { DetailGifComponent } from "./detail-gif/detail-gif.component";
import { MatPaginatorModule } from "@angular/material/paginator";

const routes = [
  {
    path: "content/list-of-gif",
    component: ListOfGifComponent
  },
  {
    path: "content/list-of-trending/detail/:id",
    component: DetailGifComponent
  },
  {
    path: "content/example",
    component: ListOfGifComponent
  },
  {
    path: "**",
    redirectTo: "content/list-of-trending"
  }
];

@NgModule({
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "fill" }
    }
  ],
  declarations: [
    ContentComponent,
    ListOfGifComponent,
    CardComponent,
    DetailGifComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    CarouselModule,
    MatPaginatorModule,
    MatIconModule
  ],
  exports: [ContentComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContentModule {}
