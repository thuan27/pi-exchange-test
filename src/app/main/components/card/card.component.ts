import { Component, OnInit, Input } from '@angular/core';
import { DataTrendingProps } from 'src/types/trending.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() listSearch: DataTrendingProps;
  constructor(private router: Router) {}

  ngOnInit() {}

  goToDetail(id: string) {
    this.router.navigateByUrl(`content/list-of-trending/detail/${id}`);
  }
}
