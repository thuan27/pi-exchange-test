export interface ParamsListOfTrendingProps {
  api_key: string;
  limit: number;
}

export interface ParamsSearchListProps {
  api_key: string;
  limit: number;
  q: string;
  offset: number;
}

export interface Pagination {
  count: number;
  offset: number;
  total_count: number;
}

export interface Image {
  height: string;
  size: string;
  url: string;
  width: string;
}

export interface ImageInformation {
  preview_webp: Image;
  downsized: Image;
}

export interface ItemTrendingProps {
  images: ImageInformation;
  url: string;
  username: string;
  import_datetime: string;
  rating: string;
  id: string;
}

export interface DataTrendingProps {
  data: Array<ItemTrendingProps>;
  status: number;
  pagination: Pagination;
}

export interface User {
  avatar_url: string;
  profile_url: string;
  description: string;
  display_name: string;
}

export interface ItemDetailProps {
  images: ImageInformation;
  url: string;
  username: string;
  import_datetime: string;
  rating: string;
  id: string;
  type: string;
  title: string;
  user: User;
}

export interface ResDetailProps {
  data: ItemDetailProps;
}
